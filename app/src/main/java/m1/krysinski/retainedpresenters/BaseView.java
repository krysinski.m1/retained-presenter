package m1.krysinski.retainedpresenters;

/**
 * Created by mich on 19/03/2017.
 */

public interface BaseView<P> {

    void setPresenter(P presenter);

}
