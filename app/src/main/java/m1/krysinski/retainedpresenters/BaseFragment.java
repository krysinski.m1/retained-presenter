package m1.krysinski.retainedpresenters;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by mich on 19/03/2017.
 */

public abstract class BaseFragment<P extends BasePresenter<V>, V extends BaseView<P>> extends Fragment {

    private static final int LOADER_ID = 1;

    private BasePresenter<V> presenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(getLoaderId(), null, new LoaderManager.LoaderCallbacks<P>() {

            @Override
            public Loader<P> onCreateLoader(int i, Bundle bundle) {
                return new PresenterLoader<P>(getActivity(), getPresenterFactory());
            }

            @Override
            public void onLoadFinished(Loader<P> loader, P p) {
                presenter = p;
                getMvpView().setPresenter(p);
            }

            @Override
            public void onLoaderReset(Loader<P> loader) {
                presenter = null;
                onPresenterDestroyed();
            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onViewAttached(getMvpView());
    }

    @Override
    public void onPause() {
        presenter.onViewDetached();
        super.onPause();
    }

    protected int getLoaderId(){
        return LOADER_ID;
    }

    protected abstract V getMvpView();

    protected abstract PresenterFactory<P> getPresenterFactory();

    protected void onPresenterDestroyed(){
    }

}
