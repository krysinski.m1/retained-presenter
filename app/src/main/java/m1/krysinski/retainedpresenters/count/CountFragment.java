package m1.krysinski.retainedpresenters.count;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import m1.krysinski.retainedpresenters.BaseFragment;
import m1.krysinski.retainedpresenters.PresenterFactory;
import m1.krysinski.retainedpresenters.R;

/**
 * Created by mich on 19/03/2017.
 */

public class CountFragment extends BaseFragment<CountContract.Presenter, CountContract.View> implements CountContract.View {

    private CountContract.Presenter presenter;

    private Unbinder unbinder;

    @BindView(R.id.fc_message)
    TextView messageView;

    public static CountFragment newInstance(){
        return new CountFragment();
    }

    @Override
    protected PresenterFactory<CountContract.Presenter> getPresenterFactory() {
        return new CountPresenterFactory();
    }

    @Override
    public void setPresenter(CountContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showMessage(String message) {
        messageView.setText(message);
    }

    @Override
    protected CountContract.View getMvpView() {
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View res = inflater.inflate(R.layout.fragment_count, container, false);
        unbinder = ButterKnife.bind(this, res);
        return res;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
