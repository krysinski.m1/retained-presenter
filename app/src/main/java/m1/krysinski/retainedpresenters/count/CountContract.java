package m1.krysinski.retainedpresenters.count;

import m1.krysinski.retainedpresenters.BasePresenter;
import m1.krysinski.retainedpresenters.BaseView;

/**
 * Created by mich on 19/03/2017.
 */

public interface CountContract {

    interface View extends BaseView<Presenter>{

        void showMessage(String message);

    }

    interface Presenter extends BasePresenter<View>{
    }

}
