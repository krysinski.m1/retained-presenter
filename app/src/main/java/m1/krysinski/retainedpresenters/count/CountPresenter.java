package m1.krysinski.retainedpresenters.count;

import java.util.Locale;

/**
 * Created by mich on 19/03/2017.
 */

public class CountPresenter implements CountContract.Presenter {

    private static final String FMT_MESSAGE = "View attached %d times.";

    private CountContract.View view;

    private int count = 0;

    @Override
    public void onViewAttached(CountContract.View view) {
        this.view = view;
        count += 1;
        this.view.showMessage(String.format(Locale.ENGLISH, FMT_MESSAGE, count));
    }

    @Override
    public void onViewDetached() {
        this.view = null;
    }

    @Override
    public void onDestroyed() {

    }
}
