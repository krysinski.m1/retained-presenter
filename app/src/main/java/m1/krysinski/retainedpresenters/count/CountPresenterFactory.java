package m1.krysinski.retainedpresenters.count;

import m1.krysinski.retainedpresenters.PresenterFactory;

/**
 * Created by mich on 19/03/2017.
 */

public class CountPresenterFactory implements PresenterFactory<CountContract.Presenter> {

    @Override
    public CountContract.Presenter create() {
        return new CountPresenter();
    }

}
