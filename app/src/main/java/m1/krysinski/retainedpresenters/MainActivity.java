package m1.krysinski.retainedpresenters;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import m1.krysinski.retainedpresenters.count.CountFragment;

public class MainActivity extends AppCompatActivity {

    private CountFragment countFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countFragment = (CountFragment) getFragmentManager().findFragmentById(R.id.am_fragment);
        if(countFragment == null){
            countFragment = CountFragment.newInstance();
            getFragmentManager().beginTransaction().add(R.id.am_fragment, countFragment).commit();
        }
    }
}
