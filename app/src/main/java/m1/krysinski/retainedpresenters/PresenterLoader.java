package m1.krysinski.retainedpresenters;

import android.content.Context;
import android.content.Loader;

/**
 * Created by mich on 19/03/2017.
 */

public class PresenterLoader<P extends BasePresenter> extends Loader<P> {

    private PresenterFactory<P> factory;

    private P presenter;

    public PresenterLoader(Context context, PresenterFactory<P> factory) {
        super(context);
        this.factory = factory;
    }

    @Override
    protected void onStartLoading() {
        if(presenter != null){
            deliverResult(presenter);
        }else{
            forceLoad();
        }
    }

    @Override
    protected void onForceLoad() {
        presenter = factory.create();
        deliverResult(presenter);
    }

    @Override
    public void deliverResult(P data) {
        super.deliverResult(data);
    }

    @Override
    public void stopLoading() {
        super.stopLoading();
    }

    @Override
    protected void onReset() {
        if(presenter != null){
            presenter.onDestroyed();
            presenter = null;
        }
    }
}
