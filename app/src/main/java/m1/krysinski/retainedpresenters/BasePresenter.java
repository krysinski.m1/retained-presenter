package m1.krysinski.retainedpresenters;

/**
 * Created by mich on 19/03/2017.
 */

public interface BasePresenter<V> {
    void onViewAttached(V view);

    void onViewDetached();

    void onDestroyed();
}
